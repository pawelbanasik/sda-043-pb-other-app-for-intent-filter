package com.example.rent.sda_042_mirror;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MirrorActivity extends AppCompatActivity {

    @BindView(R.id.text_view_output)
    protected TextView textViewOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mirror);
        ButterKnife.bind(this);

        if(getIntent() != null) {
            // caly string
//            textViewOutput.setText(getIntent().getDataString());
            // bez tego przed dwukropkiem
            textViewOutput.setText(getIntent().getData().getSchemeSpecificPart());
        } else {

            textViewOutput.setText("nie pobrano wartosci");
        }

    }
}
